import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/Team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.css']
})
export class TeamEditComponent implements OnInit {

  @Output() onEdit = new EventEmitter<number>();
  teamForEdit : Team = new Team;

  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
  }

  update(){
    this.onEdit.emit();
    this.teamForEdit = Object.assign({}, this.teamForEdit);
  }

  updateTeam(team : Team){
    return this.teamService.updateTeam(team);
  }

  loadTeam(team : Team){
    this.teamForEdit = team;
  }

  deleteTeam(id : number){
    return this.teamService.deleteTeam(id);
  }
}
