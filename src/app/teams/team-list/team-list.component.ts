import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/Team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css'],
  providers: [TeamService]
})
export class TeamListComponent implements OnInit {

  @Input()
  teams: Team[] = [];

  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });

    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });
  }

  @Output() onEdit = new EventEmitter<Team>();
  @Output() onDelete = new EventEmitter<Team>();
  @Output() onAdd = new EventEmitter<number>();
  @Output() onSave = new EventEmitter<number>();

  edit(team : Team){
    this.onEdit.emit(Object.assign({}, team));
  }

  delete(team : Team){
    this.onDelete.emit(Object.assign({}, team));
  }

  add(){
    this.onAdd.emit();
  }

  save(){
    this.onSave.emit();
  }

  getTeamNameById(id : number){
    return this.teams.find(t => t.id == id)?.name;
  }

}
