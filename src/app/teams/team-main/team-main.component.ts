import { Component, OnInit, ViewChild } from '@angular/core';
import { Team } from 'src/app/models/Team';
import { TeamCreateComponent } from '../team-create/team-create.component';
import { TeamEditComponent } from '../team-edit/team-edit.component';
import { TeamListComponent } from '../team-list/team-list.component';

@Component({
  selector: 'app-team-main',
  templateUrl: './team-main.component.html',
  styleUrls: ['./team-main.component.css']
})
export class TeamMainComponent implements OnInit {

  @ViewChild(TeamEditComponent)
  teamEditChild!: TeamEditComponent;
  @ViewChild(TeamListComponent)
  teamListChild!: TeamListComponent;
  @ViewChild(TeamCreateComponent)
  createChild!: TeamCreateComponent;
  editIsVisible = false;
  createIsVisible = true;

  teamsForEdit : Team[] = [];
  teamsIdsForDelete : number[] = [];
  teamsForCreate : Team[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onEditClick(team : Team){ //Form preparation
    this.teamEditChild.loadTeam(team);
    this.editIsVisible = true;
    this.createIsVisible = false;
  }

  onEdit(){ //Click "Edit" on form
    if(this.teamEditChild.teamForEdit.id === undefined){
      let index = this.teamsForCreate.findIndex(t => t.tempId === this.teamEditChild.teamForEdit.tempId);
      this.teamsForCreate[index] = this.teamEditChild.teamForEdit;

      index = this.teamListChild.teams.findIndex(t => t.tempId === this.teamEditChild.teamForEdit.tempId);
      this.teamListChild.teams[index] = this.teamEditChild.teamForEdit;
    }
    else{
      this.teamsForEdit.push(this.teamEditChild.teamForEdit);
      let index = this.teamListChild.teams.findIndex(u => u.id === this.teamEditChild.teamForEdit.id);
      this.teamListChild.teams[index] = this.teamEditChild.teamForEdit;
    }
  }

  onDelete(team : Team){
    if(team.id === undefined){
      let index = this.teamsForCreate.findIndex(u => u.tempId === team.tempId);
      if(index > -1)
        this.teamsForCreate.splice(index, 1);
      
      index = this.teamListChild.teams.findIndex(u => u.tempId === team.tempId);
      if(index > -1)
        this.teamListChild.teams.splice(index, 1);
    }
    else{
      this.teamsIdsForDelete.push(team.id);
      let index = this.teamListChild.teams.findIndex(u => u.id === team.id);
        if(index > -1)
          this.teamListChild.teams.splice(index, 1);
    }
  }

  onAddClick(){ //Form preparation
    this.createIsVisible = true;
    this.editIsVisible = false;
  }

  onAdd(){ //Click "Create" on form
    this.createChild.teamForCreate.tempId = this.teamsForCreate.length === 0 ? 0 : Math.max.apply(this.teamsForCreate.map(i => i.tempId)) + 1;
    this.teamsForCreate.push(this.createChild.teamForCreate);
    this.teamListChild.teams.push(this.createChild.teamForCreate);
    console.log(this.teamsForCreate);
  }

  onSave(){
    this.teamsForEdit.forEach(item => {
      this.teamEditChild.updateTeam(item).subscribe(x => {
        this.teamsForEdit = [];
      }, error => {
        console.error(error);
      });
    });

    this.teamsIdsForDelete.forEach(item => {
      this.teamEditChild.deleteTeam(item).subscribe(x =>{
        this.teamsIdsForDelete = [];
      }, error => {
        console.log(error);
      });
    });
    
    this.teamsForCreate.forEach(item => {
      this.createChild.createUser(item).subscribe(x => {
        this.teamsForCreate = [];
      }, error => {
        console.log(error);
      });
    });
  }

}
