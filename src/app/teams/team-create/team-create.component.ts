import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/models/Team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit {

  @Output() onAdd = new EventEmitter<number>();
  teamForCreate : Team = new Team;

  constructor(private teamService: TeamService) { }

  ngOnInit(): void {
  }

  create(){
    this.onAdd.emit();
  }

  createUser(team : Team){
    return this.teamService.createTeam(team);
  }
}
