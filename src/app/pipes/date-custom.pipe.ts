import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateCustom'
})
export class DateCustomPipe implements PipeTransform {

  transform(value: Date | undefined, ...args: unknown[]): string {
    if(value){
      return `${moment(value.toString()).locale('uk').format('DD MMMM YYYY')}`;
    }

    return `no date`;
  }

}
