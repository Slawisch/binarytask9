import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './not-found/not-found.component';
import { UserMainComponent } from './users/user-main/user-main.component';
import { TeamMainComponent } from './teams/team-main/team-main.component';
import { CloseUsersGuard } from './guards/users/close-users.guard';
import { CloseTeamsGuard } from './guards/teams/close-teams.guard';
import { CloseProjectsGuard } from './guards/projects/close-projects.guard';
import { ProjectMainComponent } from './projects/project-main/project-main.component';
import { TaskMainComponent } from './tasks/task-main/task-main.component';
import { CloseTasksGuard } from './guards/tasks/close-tasks.guard';

const routes: Routes = [
  { path: '', component: UserMainComponent},
  { path: 'users', component: UserMainComponent, canDeactivate: [CloseUsersGuard]},
  { path: 'teams', component: TeamMainComponent, canDeactivate: [CloseTeamsGuard]},
  { path: 'projects', component: ProjectMainComponent, canDeactivate: [CloseProjectsGuard]},
  { path: 'tasks', component: TaskMainComponent, canDeactivate: [CloseTasksGuard]},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
