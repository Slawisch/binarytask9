import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { Team } from 'src/app/models/Team';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit {

  @Output() onEdit = new EventEmitter<number>();
  projectForEdit : Project = new Project;
  users : User[] = [];
  teams : Team[] = [];

  constructor(private projectService: ProjectService, private userService: UserService, private teamService: TeamService) { }

  ngOnInit(): void {
  }

  update(){
    this.onEdit.emit();
    this.projectForEdit = Object.assign({}, this.projectForEdit);
  }

  updateProject(project : Project){
    return this.projectService.updateProject(project);
  }

  loadProject(project : Project){
    this.projectForEdit = project;

    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });

    this.userService.getUsers().pipe()
    .subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });
  }

  deleteProject(id : number){
    return this.projectService.deleteProject(id);
  }
}
