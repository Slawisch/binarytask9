import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { Team } from 'src/app/models/Team';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.css']
})
export class ProjectListComponent implements OnInit {

  @Input()
  projects: Project[] = [];
  users: User[] = [];
  teams: Team[] = [];

  constructor(private projectService: ProjectService, private userService: UserService, private teamService: TeamService) { }

  ngOnInit(): void {
    this.projectService.getProjects().pipe()
    .subscribe(x => {
      this.projects = x;
    }, (error) => {
      console.log(error)
    });

    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });

    this.userService.getUsers().pipe()
    .subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });
  }

  @Output() onEdit = new EventEmitter<Project>();
  @Output() onDelete = new EventEmitter<Project>();
  @Output() onAdd = new EventEmitter<number>();
  @Output() onSave = new EventEmitter<number>();

  edit(project : Project){
    this.onEdit.emit(Object.assign({}, project));
  }

  delete(project : Project){
    this.onDelete.emit(Object.assign({}, project));
  }

  add(){
    this.onAdd.emit();
  }

  save(){
    this.onSave.emit();
  }

  getTeamNameById(id : number){
    return this.teams.find(t => t.id == id)?.name;
  }

  getUserNameById(id : number){
    let user = this.users.find(u => u.id == id)
    return user?.firstName + " " + user?.lastName;
  }

}
