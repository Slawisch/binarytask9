import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { Team } from 'src/app/models/Team';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit {

  @Output() onAdd = new EventEmitter<number>();
  projectForCreate : Project = new Project;
  users : User[] = [];
  teams : Team[] = [];

  constructor(private projectService: ProjectService, private userService: UserService, private teamService: TeamService) { }

  ngOnInit(): void {
    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });

    this.userService.getUsers().pipe()
    .subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });
  }

  create(){
    this.onAdd.emit();
  }

  createProject(project : Project){
    return this.projectService.createProject(project);
  }

}
