import { Component, OnInit, ViewChild } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { ProjectCreateComponent } from '../project-create/project-create.component';
import { ProjectEditComponent } from '../project-edit/project-edit.component';
import { ProjectListComponent } from '../project-list/project-list.component';

@Component({
  selector: 'app-project-main',
  templateUrl: './project-main.component.html',
  styleUrls: ['./project-main.component.css']
})
export class ProjectMainComponent implements OnInit {

  @ViewChild(ProjectEditComponent)
  projectEditChild!: ProjectEditComponent;
  @ViewChild(ProjectListComponent)
  projectListChild!: ProjectListComponent;
  @ViewChild(ProjectCreateComponent)
  createChild!: ProjectCreateComponent;
  editIsVisible = false;
  createIsVisible = true;

  projectsForEdit : Project[] = [];
  projectsIdsForDelete : number[] = [];
  projectsForCreate : Project[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onEditClick(project : Project){ //Form preparation
    this.projectEditChild.loadProject(project);
    this.editIsVisible = true;
    this.createIsVisible = false;
  }

  onEdit(){ //Click "Edit" on form
    if(this.projectEditChild.projectForEdit.id === undefined){
      let index = this.projectsForCreate.findIndex(p => p.tempId === this.projectEditChild.projectForEdit.tempId);
      this.projectsForCreate[index] = this.projectEditChild.projectForEdit;

      index = this.projectListChild.projects.findIndex(p => p.tempId === this.projectEditChild.projectForEdit.tempId);
      this.projectListChild.projects[index] = this.projectEditChild.projectForEdit;
    }
    else{
      this.projectsForEdit.push(this.projectEditChild.projectForEdit);
      let index = this.projectListChild.projects.findIndex(p => p.id === this.projectEditChild.projectForEdit.id);
      this.projectListChild.projects[index] = this.projectEditChild.projectForEdit;
    }
  }

  onDelete(project : Project){
    if(project.id === undefined){
      let index = this.projectsForCreate.findIndex(u => u.tempId === project.tempId);
      if(index > -1)
        this.projectsForCreate.splice(index, 1);
      
      index = this.projectListChild.projects.findIndex(u => u.tempId === project.tempId);
      if(index > -1)
        this.projectListChild.projects.splice(index, 1);
    }
    else{
      this.projectsIdsForDelete.push(project.id);
      let index = this.projectListChild.projects.findIndex(u => u.id === project.id);
        if(index > -1)
          this.projectListChild.projects.splice(index, 1);
    }
  }

  onAddClick(){ //Form preparation
    this.createIsVisible = true;
    this.editIsVisible = false;
  }

  onAdd(){ //Click "Create" on form
    this.createChild.projectForCreate.tempId = this.projectsForCreate.length === 0 ? 0 : Math.max.apply(this.projectsForCreate.map(i => i.tempId)) + 1;
    this.projectsForCreate.push(this.createChild.projectForCreate);
    this.projectListChild.projects.push(this.createChild.projectForCreate);
    console.log(this.projectsForCreate);
  }

  onSave(){
    this.projectsForEdit.forEach(item => {
      this.projectEditChild.updateProject(item).subscribe(x => {
        this.projectsForEdit = [];
      }, error => {
        console.error(error);
      });
    });

    this.projectsIdsForDelete.forEach(item => {
      this.projectEditChild.deleteProject(item).subscribe(x =>{
        this.projectsIdsForDelete = [];
      }, error => {
        console.log(error);
      });
    });
    
    this.projectsForCreate.forEach(item => {
      this.createChild.createProject(item).subscribe(x => {
        this.projectsForCreate = [];
      }, error => {
        console.log(error);
      });
    });
  }
}
