import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { UserMainComponent } from './users/user-main/user-main.component';
import { UserCreateComponent } from './users/user-create/user-create.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DateCustomPipe } from './pipes/date-custom.pipe';
import { TaskMainComponent } from './tasks/task-main/task-main.component';
import { TaskListComponent } from './tasks/task-list/task-list.component';
import { TaskCreateComponent } from './tasks/task-create/task-create.component';
import { TaskEditComponent } from './tasks/task-edit/task-edit.component';
import { TeamMainComponent } from './teams/team-main/team-main.component';
import { TeamListComponent } from './teams/team-list/team-list.component';
import { TeamCreateComponent } from './teams/team-create/team-create.component';
import { TeamEditComponent } from './teams/team-edit/team-edit.component';
import { ProjectEditComponent } from './projects/project-edit/project-edit.component';
import { ProjectCreateComponent } from './projects/project-create/project-create.component';
import { ProjectListComponent } from './projects/project-list/project-list.component';
import { ProjectMainComponent } from './projects/project-main/project-main.component';
import { TaskStateDirective } from './directives/task-state.directive';

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    NotFoundComponent,
    EditUserComponent,
    UserMainComponent,
    UserCreateComponent,
    NavigationComponent,
    DateCustomPipe,
    TaskMainComponent,
    TaskListComponent,
    TaskCreateComponent,
    TaskEditComponent,
    TeamMainComponent,
    TeamListComponent,
    TeamCreateComponent,
    TeamEditComponent,
    ProjectEditComponent,
    ProjectCreateComponent,
    ProjectListComponent,
    ProjectMainComponent,
    TaskStateDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
