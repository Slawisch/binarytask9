import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ProjectMainComponent } from 'src/app/projects/project-main/project-main.component';

@Injectable({
  providedIn: 'root'
})
export class CloseProjectsGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: ProjectMainComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return (
        component.projectsForCreate.length === 0 && 
        component.projectsForEdit.length === 0 && 
        component.projectsIdsForDelete.length === 0
        ) 
        || window.confirm("You have unsaved changes. Do you want to leave?");
  }
  
}
