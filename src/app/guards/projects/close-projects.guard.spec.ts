import { TestBed } from '@angular/core/testing';

import { CloseProjectsGuard } from './close-projects.guard';

describe('CloseProjectsGuard', () => {
  let guard: CloseProjectsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CloseProjectsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
