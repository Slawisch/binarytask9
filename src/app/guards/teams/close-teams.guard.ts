import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TeamMainComponent } from 'src/app/teams/team-main/team-main.component';

@Injectable({
  providedIn: 'root'
})
export class CloseTeamsGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: TeamMainComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return (
        component.teamsForCreate.length === 0 && 
        component.teamsForEdit.length === 0 && 
        component.teamsIdsForDelete.length === 0
        ) 
        || window.confirm("You have unsaved changes. Do you want to leave?");
  }
  
}
