import { TestBed } from '@angular/core/testing';

import { CloseTeamsGuard } from './close-teams.guard';

describe('CloseTeamsGuard', () => {
  let guard: CloseTeamsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CloseTeamsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
