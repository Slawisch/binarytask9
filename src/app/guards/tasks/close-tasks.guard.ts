import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TaskMainComponent } from 'src/app/tasks/task-main/task-main.component';

@Injectable({
  providedIn: 'root'
})
export class CloseTasksGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: TaskMainComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return (
        component.tasksForCreate.length === 0 && 
        component.tasksForEdit.length === 0 && 
        component.tasksIdsForDelete.length === 0
        ) 
        || window.confirm("You have unsaved changes. Do you want to leave?");
  }
  
}
