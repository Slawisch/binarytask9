import { TestBed } from '@angular/core/testing';

import { CloseTasksGuard } from './close-tasks.guard';

describe('CloseTasksGuard', () => {
  let guard: CloseTasksGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CloseTasksGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
