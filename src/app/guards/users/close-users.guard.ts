import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UserMainComponent } from 'src/app/users/user-main/user-main.component';

@Injectable({
  providedIn: 'root'
})
export class CloseUsersGuard implements CanDeactivate<unknown> {
  canDeactivate(
    component: UserMainComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return (
        component.usersForCreate.length === 0 && 
        component.usersForEdit.length === 0 && 
        component.usersIdsForDelete.length === 0
        ) 
        || window.confirm("You have unsaved changes. Do you want to leave?");
  }
  
}
