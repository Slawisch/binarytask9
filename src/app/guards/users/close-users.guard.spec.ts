import { TestBed } from '@angular/core/testing';

import { CloseUsersGuard } from './close-users.guard';

describe('CloseUsersGuard', () => {
  let guard: CloseUsersGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(CloseUsersGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
