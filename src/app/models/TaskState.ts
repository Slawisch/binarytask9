export enum TaskStates{
    ToDo,
    InProgress,
    Done,
    Canceled
}