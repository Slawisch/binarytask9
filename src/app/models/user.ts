export class User {
    id! : number;
    teamId! : number;
    firstName? : string;
    lastName? : string;
    email? : string;
    birthDay? : Date;
    registeredAt? : Date;
    tempId! : number;
}