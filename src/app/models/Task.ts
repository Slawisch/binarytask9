import { TaskStates } from './TaskState';

export class TaskModel {
    id! : number;
    projectId! : number;
    performerId! : number;
    name? : string;
    description? : string;
    state?: TaskStates;
    finishedAt? : Date;
    createdAt? : Date;
    tempId! : number;
}