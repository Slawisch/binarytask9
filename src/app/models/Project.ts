export class Project {
    id! : number;
    teamId! : number;
    authorId! : number;
    name? : string;
    description? : string;
    deadline? : Date;
    createdAt? : Date;
    tempId! : number;
}