import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TaskModel } from '../models/Task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private routePrefix = 'https://localhost:44322/api/tasks';

  constructor(private http: HttpClient) { }

  getTasks() : Observable<TaskModel[]>{
    return this.http.get<TaskModel[]>(`${this.routePrefix}`);
  }

  getTaskById(id : number){
    return this.http.get<TaskModel>(`${this.routePrefix}/${id}`);
  }

  updateTask(task : TaskModel){
    return this.http.put<TaskModel>(`${this.routePrefix}`, task);
  }

  deleteTask(id : number){
    return this.http.delete(`${this.routePrefix}/${id}`);
  }

  createTask(task : TaskModel){
    return this.http.post(`${this.routePrefix}`, task);
  }
}
