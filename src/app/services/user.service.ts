import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  private routePrefix = 'https://localhost:44322/api/users';

  constructor(private http: HttpClient) { }

  getUsers() : Observable<User[]>{
    return this.http.get<User[]>(`${this.routePrefix}`);
  }

  getUserById(id : number){
    return this.http.get<User>(`${this.routePrefix}/${id}`);
  }

  updateUser(user : User){
    return this.http.put<User>(`${this.routePrefix}`, user);
  }

  deleteUser(id : number){
    return this.http.delete(`${this.routePrefix}/${id}`);
  }

  createUser(user : User){
    return this.http.post(`${this.routePrefix}`, user);
  }
}
