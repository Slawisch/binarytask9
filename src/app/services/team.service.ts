import { Injectable } from '@angular/core';
import { Team } from '../models/Team';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class TeamService {

  private routePrefix = 'https://localhost:44322/api/teams';

  constructor(private http: HttpClient) { }

  getTeams() : Observable<Team[]>{
    return this.http.get<Team[]>(`${this.routePrefix}`);
  }

  getTeamById(id : number){
    return this.http.get<Team>(`${this.routePrefix}/${id}`);
  }

  updateTeam(team : Team){
    return this.http.put<Team>(`${this.routePrefix}`, team);
  }

  deleteTeam(id : number){
    return this.http.delete(`${this.routePrefix}/${id}`);
  }

  createTeam(team : Team){
    return this.http.post(`${this.routePrefix}`, team);
  }
}
