import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Project } from '../models/Project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private routePrefix = 'https://localhost:44322/api/projects';

  constructor(private http: HttpClient) { }

  getProjects() : Observable<Project[]>{
    return this.http.get<Project[]>(`${this.routePrefix}`);
  }

  getProjectById(id : number){
    return this.http.get<Project>(`${this.routePrefix}/${id}`);
  }

  updateProject(project : Project){
    return this.http.put<Project>(`${this.routePrefix}`, project);
  }

  deleteProject(id : number){
    return this.http.delete(`${this.routePrefix}/${id}`);
  }

  createProject(project : Project){
    return this.http.post(`${this.routePrefix}`, project);
  }
}
