import { Directive, ElementRef, HostBinding, HostListener, Input, OnInit } from '@angular/core';
import { TaskStates } from '../models/TaskState';

@Directive({
  selector: '[appTaskState]'
})
export class TaskStateDirective implements OnInit{

  @Input() state : TaskStates | undefined;

  constructor(private el : ElementRef) {
    console.log(el);
  }

  ngOnInit(): void {
    console.log(this.state);
      if(this.state == 0){
        this.el.nativeElement.style.backgroundColor="#aaaaaa";
        this.el.nativeElement.setAttribute('title', "To Do");
      }
      else if(this.state == 1){
        this.el.nativeElement.style.backgroundColor="#ded159";
        this.el.nativeElement.setAttribute('title', "In Progress");
      }
      else if(this.state == 2){
        this.el.nativeElement.style.backgroundColor="#36c25b";
        this.el.nativeElement.setAttribute('title', "Done");
      }
      else if(this.state == 3){
        this.el.nativeElement.style.backgroundColor="#d13d3d";
        this.el.nativeElement.setAttribute('title', "Canceled");
      }

        console.log(this.state);
        console.log(TaskStates.Done);
        console.log(this.state == TaskStates.Done);
  }

  

}