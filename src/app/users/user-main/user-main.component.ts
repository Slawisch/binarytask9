import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { UserCreateComponent } from '../user-create/user-create.component';
import { UserListComponent } from '../user-list/user-list.component';

@Component({
  selector: 'app-user-main',
  templateUrl: './user-main.component.html',
  styleUrls: ['./user-main.component.css']
})
export class UserMainComponent implements OnInit {

  @ViewChild(EditUserComponent)
  userEditChild!: EditUserComponent;
  @ViewChild(UserListComponent)
  userListChild!: UserListComponent;
  @ViewChild(UserCreateComponent)
  createChild!: UserCreateComponent;
  editIsVisible = false;
  createIsVisible = true;

  usersForEdit : User[] = [];
  usersIdsForDelete : number[] = [];
  usersForCreate : User[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onEditClick(user : User){ //Form preparation
    this.userEditChild.loadUser(user);
    this.editIsVisible = true;
    this.createIsVisible = false;
  }

  onEdit(){ //Click "Edit" on form
    if(this.userEditChild.userForEdit.id === undefined){
      let index = this.usersForCreate.findIndex(u => u.tempId === this.userEditChild.userForEdit.tempId);
      this.usersForCreate[index] = this.userEditChild.userForEdit;

      index = this.userListChild.users.findIndex(u => u.tempId === this.userEditChild.userForEdit.tempId);
      this.userListChild.users[index] = this.userEditChild.userForEdit;
    }
    else{
      this.usersForEdit.push(this.userEditChild.userForEdit);
      let index = this.userListChild.users.findIndex(u => u.id === this.userEditChild.userForEdit.id);
      this.userListChild.users[index] = this.userEditChild.userForEdit;
    }
  }

  onDelete(user : User){
    if(user.id === undefined){
      let index = this.usersForCreate.findIndex(u => u.tempId === user.tempId);
      if(index > -1)
        this.usersForCreate.splice(index, 1);
      
      index = this.userListChild.users.findIndex(u => u.tempId === user.tempId);
      if(index > -1)
        this.userListChild.users.splice(index, 1);
    }
    else{
      this.usersIdsForDelete.push(user.id);
      let index = this.userListChild.users.findIndex(u => u.id === user.id);
        if(index > -1)
          this.userListChild.users.splice(index, 1);
    }
  }

  onAddClick(){ //Form preparation
    this.createIsVisible = true;
    this.editIsVisible = false;
  }

  onAdd(){ //Click "Create" on form
    this.createChild.userForCreate.tempId = this.usersForCreate.length === 0 ? 0 : Math.max.apply(this.usersForCreate.map(i => i.tempId)) + 1;
    this.usersForCreate.push(this.createChild.userForCreate);
    this.userListChild.users.push(this.createChild.userForCreate);
    console.log(this.usersForCreate);
  }

  onSave(){
    this.usersForEdit.forEach(item => {
      this.userEditChild.updateUser(item).subscribe(x => {
        this.usersForEdit = [];
      }, error => {
        console.error(error);
      });
    });

    this.usersIdsForDelete.forEach(item => {
      this.userEditChild.deleteUser(item).subscribe(x =>{
        this.usersIdsForDelete = [];
      }, error => {
        console.log(error);
      });
    });
    
    this.usersForCreate.forEach(item => {
      this.createChild.createUser(item).subscribe(x => {
        this.usersForCreate = [];
      }, error => {
        console.log(error);
      });
    });

  }
}
