import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Team } from 'src/app/models/Team';
import { User } from 'src/app/models/user';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
  providers: [UserService, TeamService]
})
export class UserListComponent implements OnInit {
  @Input()
  users: User[] = [];
  teams: Team[] = [];

  constructor(private userService: UserService, private teamService: TeamService) { }

  ngOnInit(): void {
    this.userService.getUsers().pipe()
    .subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });

    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });
  }

  @Output() onEdit = new EventEmitter<User>();
  @Output() onDelete = new EventEmitter<User>();
  @Output() onAdd = new EventEmitter<number>();
  @Output() onSave = new EventEmitter<number>();

  edit(user : User){
    this.onEdit.emit(Object.assign({}, user));
  }

  delete(user : User){
    this.onDelete.emit(Object.assign({}, user));
  }

  add(){
    this.onAdd.emit();
  }

  save(){
    this.onSave.emit();
  }

  getTeamNameById(id : number){
    return this.teams.find(t => t.id == id)?.name;
  }
}
