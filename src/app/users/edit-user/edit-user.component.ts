import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { Observable } from 'rxjs';
import { Team } from 'src/app/models/Team';
import { User } from 'src/app/models/user';
import { TeamService } from 'src/app/services/team.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css'],
  providers: [UserService, TeamService]
})
export class EditUserComponent implements OnInit {

  @Output() onEdit = new EventEmitter<number>();
  userForEdit : User = new User;
  teams : Team[] = [];

  constructor(private userService: UserService, private teamService: TeamService) { }

  ngOnInit(): void {
  }

  update(){
    this.onEdit.emit();
    this.userForEdit = Object.assign({}, this.userForEdit);
  }

  updateUser(user : User){
    return this.userService.updateUser(user);
  }

  loadUser(user : User){
    this.userForEdit = user;

    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });
  }

  deleteUser(id : number){
    return this.userService.deleteUser(id);
  }
}
