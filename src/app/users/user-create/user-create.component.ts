import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/models/user';
import { Team } from 'src/app/models/Team';
import { TeamService } from 'src/app/services/team.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css'],
  providers: [UserService, TeamService]
})
export class UserCreateComponent implements OnInit {

  @Output() onAdd = new EventEmitter<number>();
  userForCreate : User = new User;
  teams : Team[] = [];

  constructor(private userService: UserService, private teamService: TeamService) { }

  ngOnInit(): void {
    this.teamService.getTeams().pipe()
    .subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });
  }

  create(){
    this.onAdd.emit();
  }

  createUser(user : User){
    return this.userService.createUser(user);
  }
}
