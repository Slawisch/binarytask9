import { Component, OnInit, ViewChild } from '@angular/core';
import { TaskModel } from 'src/app/models/Task';
import { TaskCreateComponent } from '../task-create/task-create.component';
import { TaskEditComponent } from '../task-edit/task-edit.component';
import { TaskListComponent } from '../task-list/task-list.component';

@Component({
  selector: 'app-task-main',
  templateUrl: './task-main.component.html',
  styleUrls: ['./task-main.component.css']
})
export class TaskMainComponent implements OnInit {

  
  @ViewChild(TaskEditComponent)
  taskEditChild!: TaskEditComponent;
  @ViewChild(TaskListComponent)
  taskListChild!: TaskListComponent;
  @ViewChild(TaskCreateComponent)
  createChild!: TaskCreateComponent;
  editIsVisible = false;
  createIsVisible = true;

  tasksForEdit : TaskModel[] = [];
  tasksIdsForDelete : number[] = [];
  tasksForCreate : TaskModel[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  onEditClick(task : TaskModel){ //Form preparation
    this.taskEditChild.loadTask(task);
    this.editIsVisible = true;
    this.createIsVisible = false;
  }

  onEdit(){ //Click "Edit" on form
    if(this.taskEditChild.taskForEdit.id === undefined){
      let index = this.tasksForCreate.findIndex(p => p.tempId === this.taskEditChild.taskForEdit.tempId);
      this.tasksForCreate[index] = this.taskEditChild.taskForEdit;

      index = this.taskListChild.tasks.findIndex(p => p.tempId === this.taskEditChild.taskForEdit.tempId);
      this.taskListChild.tasks[index] = this.taskEditChild.taskForEdit;
    }
    else{
      this.tasksForEdit.push(this.taskEditChild.taskForEdit);
      let index = this.taskListChild.tasks.findIndex(p => p.id === this.taskEditChild.taskForEdit.id);
      this.taskListChild.tasks[index] = this.taskEditChild.taskForEdit;
    }
  }

  onDelete(task : TaskModel){
    if(task.id === undefined){
      let index = this.tasksForCreate.findIndex(u => u.tempId === task.tempId);
      if(index > -1)
        this.tasksForCreate.splice(index, 1);
      
      index = this.taskListChild.tasks.findIndex(u => u.tempId === task.tempId);
      if(index > -1)
        this.taskListChild.tasks.splice(index, 1);
    }
    else{
      this.tasksIdsForDelete.push(task.id);
      let index = this.taskListChild.tasks.findIndex(u => u.id === task.id);
        if(index > -1)
          this.taskListChild.tasks.splice(index, 1);
    }
  }

  onAddClick(){ //Form preparation
    this.createIsVisible = true;
    this.editIsVisible = false;
  }

  onAdd(){ //Click "Create" on form
    this.createChild.taskForCreate.tempId = this.tasksForCreate.length === 0 ? 0 : Math.max.apply(this.tasksForCreate.map(i => i.tempId)) + 1;
    this.tasksForCreate.push(this.createChild.taskForCreate);
    this.taskListChild.tasks.push(this.createChild.taskForCreate);
    console.log(this.tasksForCreate);
  }

  onSave(){
    this.tasksForEdit.forEach(item => {
      this.taskEditChild.updateTask(item).subscribe(x => {
        this.tasksForEdit = [];
      }, error => {
        console.error(error);
      });
    });

    this.tasksIdsForDelete.forEach(item => {
      this.taskEditChild.deleteTask(item).subscribe(x =>{
        this.tasksIdsForDelete = [];
      }, error => {
        console.log(error);
      });
    });
    
    this.tasksForCreate.forEach(item => {
      this.createChild.createTask(item).subscribe(x => {
        this.tasksForCreate = [];
      }, error => {
        console.log(error);
      });
    });
  }
  
}
