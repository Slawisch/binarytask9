import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { TaskModel } from 'src/app/models/Task';
import { TaskStates } from 'src/app/models/TaskState';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {

  @Output() onEdit = new EventEmitter<number>();
  taskForEdit : TaskModel = new TaskModel;
  users : User[] = [];
  projects : Project[] = [];
  states = TaskStates;

  constructor(private projectService: ProjectService, private userService: UserService, private taskService: TaskService) { }

  ngOnInit(): void {
  }

  update(){
    this.onEdit.emit();
    this.taskForEdit = Object.assign({}, this.taskForEdit);
  }

  updateTask(task : TaskModel){
    return this.taskService.updateTask(task);
  }

  loadTask(task : TaskModel){
    this.taskForEdit = task;

    this.projectService.getProjects().pipe()
    .subscribe(x => {
      this.projects = x;
    }, (error) => {
      console.log(error)
    });

    this.userService.getUsers().pipe()
    .subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });
  }

  deleteTask(id : number){
    return this.taskService.deleteTask(id);
  }
}
