import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { TaskModel } from 'src/app/models/Task';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  @Input()
  projects: Project[] = [];
  users: User[] = [];
  tasks: TaskModel[] = [];

  constructor(private projectService: ProjectService, private userService: UserService, private taskService: TaskService) { }

  ngOnInit(): void {
    this.projectService.getProjects().pipe()
    .subscribe(x => {
      this.projects = x;
    }, (error) => {
      console.log(error)
    });

    this.taskService.getTasks().pipe()
    .subscribe(x => {
      this.tasks = x;
    }, (error) => {
      console.log(error)
    });

    this.userService.getUsers().pipe()
    .subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });
  }

  @Output() onEdit = new EventEmitter<TaskModel>();
  @Output() onDelete = new EventEmitter<TaskModel>();
  @Output() onAdd = new EventEmitter<number>();
  @Output() onSave = new EventEmitter<number>();

  edit(task : TaskModel){
    this.onEdit.emit(Object.assign({}, task));
  }

  delete(task : TaskModel){
    this.onDelete.emit(Object.assign({}, task));
  }

  add(){
    this.onAdd.emit();
  }

  save(){
    this.onSave.emit();
  }

  getProjectNameById(id : number){
    return this.projects.find(p => p.id == id)?.name;
  }

  getUserNameById(id : number){
    let user = this.users.find(u => u.id == id)
    return user?.firstName + " " + user?.lastName;
  }

}
