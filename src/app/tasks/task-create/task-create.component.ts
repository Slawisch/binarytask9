import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Project } from 'src/app/models/Project';
import { TaskModel } from 'src/app/models/Task';
import { TaskStates } from 'src/app/models/TaskState';
import { User } from 'src/app/models/user';
import { ProjectService } from 'src/app/services/project.service';
import { TaskService } from 'src/app/services/task.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit {

  @Output() onAdd = new EventEmitter<number>();
  taskForCreate : TaskModel = new TaskModel;
  users : User[] = [];
  projects : Project[] = [];
  states = TaskStates;

  constructor(private projectService: ProjectService, private userService: UserService, private taskService: TaskService) { }

  ngOnInit(): void {
    this.projectService.getProjects().pipe()
    .subscribe(x => {
      this.projects = x;
    }, (error) => {
      console.log(error)
    });

    this.userService.getUsers().pipe()
    .subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });
  }

  create(){
    this.onAdd.emit();
  }

  createTask(task : TaskModel){
    return this.taskService.createTask(task);
  }

}
